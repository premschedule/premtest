This README file explains how to use the program which is an event organisation.

Purpose
-----------

This is a Java event organisation application.
Organisers have provided you with the following useful information:
1. The employees will be divided into various teams and each team will be performing various activities
2. Activities start from 9am until lunch is served at 12 noon
3. Activities resume from 1pm and must finish in time for a presentation to be delivered by external speaker on “Staff Motivation" 4. The presentation can start no earlier than 4:00 and no later than 5:00
5. All activity lengths are either in minutes (not hours) or sprint (15 minutes)
6. Due to high spirit of the team there needs to be no gap between each activity

Installation - Use IDE[ I have used IntelliJ]
-----------
1) Checkout the project in your system.
1) Import the project into your IDE
2) Run the main class "DayAwayMain.main()" found in path com/eventscheduler.DayAwayMain

Design
-----------

- There is a scheduler located at com.eventscheduler.scheduler.ActivityScheduler and associated junit ActivitySchedulerTest.java
- Model : POJO class to hold the details of Activity and team
- Enum  : ActivityDuration
- Util  : There are utils like mapper which converts the activities.txt file into java objects
          There is a file for parsing, printing the result and duration converter
- Exception : For handling custom exception
- Main Program: Located at com.eventscheduler.DayAwayMain.java

- Miscellaneous: Few external jars has been added for creating unit test cases and apache utils.

Assumptions
-----------

Activities cannot be repeated
No two teams can have the same activities at the same time.
Lunch is served at 12 but the teams may begin heading for lunch few minutes before 12

Output
-----------

If program runs successfully, it will display the below result.

Activities for Team 1
09:00 AM Archery PT45M
09:45 AM Learning Magic Tricks PT40M
10:25 AM Human Table Football PT30M
10:55 AM Duck Herding PT1H
11:55 AM Lunch PT1H
12:55 PM Time Tracker PT15M
01:10 PM Wine Tasting PT15M
01:25 PM Giant Digital Graffiti PT1H
02:25 PM Viking Axe Throwing PT1H
03:25 PM Arduino Bonanza PT30M
03:55 PM Cricket 2020 PT1H
04:55 PM Staff Motivation Presentation PT1H



Activities for Team 2
09:00 AM 2-wheeled Segways PT45M
09:45 AM Salsa & Pickles PT15M
10:00 AM Buggy Driving PT30M
10:30 AM Laser Clay Shooting PT1H
11:30 AM Giant Puzzle Dinosaurs PT30M
12:00 PM Lunch PT1H
01:00 PM Enigma Challenge PT45M
01:45 PM Indiano Drizzle PT45M
02:30 PM New Zealand Haka PT30M
03:00 PM Digital Tresure Hunt PT1H
04:00 PM Monti Carlo or Bust PT1H
05:00 PM Staff Motivation Presentation PT1H





