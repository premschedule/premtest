package com.eventscheduler.util;


import static com.eventscheduler.util.ActivityMapper.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.eventscheduler.exception.ActivityHandlerException;
import com.eventscheduler.model.Activity;



/**
 * The type Activity file parser.
 */
public class ActivityFileParser
{

	private String fileName;

	/**
	 * Instantiates a new Activity file parser.
	 *
	 * @param fileName
	 * 		the file name
	 */
	public ActivityFileParser(final String fileName)
	{
		this.fileName = fileName;
	}

	/**
	 * Parse list.
	 *
	 * @return the list
	 * @throws IOException
	 * 		the io exception
	 * @throws ActivityHandlerException
	 * 		the activity handler exception
	 */
	public List<Activity> parse() throws IOException, ActivityHandlerException
	{
		List<Activity> activities = new ArrayList<>();
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try
		{
			final File file = new File(getFileName());
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			for (String line; (line = bufferedReader.readLine()) != null; )
			{
				activities.add(map(line));
			}
		}
		catch (IOException e)
		{
			throw new ActivityHandlerException("Error reading input file: " + fileName, e);
		}
		finally
		{
			fileReader.close();
			bufferedReader.close();
		}
		return activities;
	}

	/**
	 * Gets file name.
	 *
	 * @return the file name
	 */
	public String getFileName()
	{
		return fileName;
	}

}
