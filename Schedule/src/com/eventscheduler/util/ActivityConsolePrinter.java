package com.eventscheduler.util;



import com.eventscheduler.model.Activity;


/**
 * The type Activity console printer.
 */
public class ActivityConsolePrinter
{

	/**
	 * Print.
	 *
	 * @param time
	 * 		the time
	 * @param activity
	 * 		the activity
	 */
	public static void print(final String time, final Activity activity)
	{
		System.out.println(time + " " + activity.getName() + " " + activity.getDuration());
	}
}
