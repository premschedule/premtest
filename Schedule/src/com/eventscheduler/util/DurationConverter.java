package com.eventscheduler.util;

import java.time.Duration;


/**
 * The type Duration converter.
 */
public final class DurationConverter
{

	private static final String MINS = "min";
	private static final String SPRINT = "sprint";

	/**
	 * Convert duration.
	 *
	 * @param string
	 * 		the string
	 *
	 * @return the duration
	 */
	public static Duration convert(final String string)
	{
		Duration duration = null;
		if (string == null)
		{
			duration = Duration.ZERO;
		}
		else if (string.equals(SPRINT))
		{
			duration = Duration.ofMinutes(15);
		}
		else if (string.endsWith(MINS))
		{
			duration = Duration.ofMinutes(Integer.valueOf(string.substring(0, string.indexOf(MINS))));
		}
		return duration;
	}
}
