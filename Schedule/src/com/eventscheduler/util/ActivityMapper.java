package com.eventscheduler.util;

import java.time.Duration;

import com.eventscheduler.model.Activity;


/**
 * The type Activity mapper.
 */
public class ActivityMapper
{

	/**
	 * Map activity.
	 *
	 * @param activityRow
	 * 		the activity row
	 *
	 * @return the activity
	 */
	public static Activity map(final String activityRow)
	{
		Activity activity = null;
		if (activityRow != null)
		{
			String[] tokens = activityRow.split(" ");
			String durationString = tokens[tokens.length - 1];
			String name = activityRow.replace(durationString, "");
			Duration duration = DurationConverter.convert(durationString);
			activity = new Activity(name.trim(), duration);
		}
		return activity;
	}
}
