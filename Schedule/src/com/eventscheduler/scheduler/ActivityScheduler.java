package com.eventscheduler.scheduler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import com.eventscheduler.enums.ActivityDuration;
import com.eventscheduler.model.Activity;
import com.eventscheduler.model.Team;


/**
 * The type Activity scheduler.
 */
public class ActivityScheduler
{
	private List<Activity> activities;

	/**
	 * Schedule list.
	 *
	 * @param teamSize
	 * 		the team size
	 * @param activities
	 * 		the activities
	 *
	 * @return the list
	 */
	public List<Team> schedule(final int teamSize, final List<Activity> activities)
	{
		final List<Team> teams = new ArrayList<>();
		setActivities(activities);

		for (int i = 0; i < teamSize; i++)
		{
			teams.add(new Team("Team" + (i + 1)));
		}
		if (CollectionUtils.isNotEmpty(teams))
		{
			scheduleActivitiesPreLunch(teams);
			scheduleActivitiesPostLunch(teams);
		}
		return teams;
	}

	/**
	 * Schedule activities pre lunch.
	 *
	 * @param teams
	 * 		the teams
	 */
	protected void scheduleActivitiesPreLunch(final List<Team> teams)
	{
 		teams.forEach(team ->
		{
			final ActivityDuration activityDuration = team.getDuration();
			final Set<Activity> selectedActivities = new HashSet<>();
			for (final Iterator<Activity> iterator = getActivities().iterator(); iterator.hasNext(); )
			{
				Activity activity = iterator.next();
				//check that it fits the duration we need
				selectedActivities.add(activity);
				if (CollectionUtils.isNotEmpty(selectedActivities) && activityDurationSum(selectedActivities) <= activityDuration
						.getMaxMins())
				{
					iterator.remove();
				}
				else
				{
					selectedActivities.remove(activity);
				}
				if (activityDurationSum(selectedActivities) > activityDuration.getMinMins())
				{
					break;
				}
			}
			team.addActivities(selectedActivities);
		});
	}

	/**
	 * Schedule activities post lunch.
	 *
	 * @param teams
	 * 		the teams
	 */
	protected void scheduleActivitiesPostLunch(final List<Team> teams)
	{
		teams.forEach(team ->
		{
			final long remainingTime = team.getDuration().getTotalMins() - activityDurationSum(team.getMorningActivities());
			final Set<Activity> selectedActivities = new HashSet<>();
			for (final Iterator<Activity> iterator = getActivities().iterator(); iterator.hasNext(); )
			{
				final Activity activity = iterator.next();
				//check that it fits the duration we need
				selectedActivities.add(activity);
				if (CollectionUtils.isNotEmpty(selectedActivities) && activityDurationSum(selectedActivities) <= remainingTime && CollectionUtils
						.isNotEmpty(team.getMorningActivities()) && !team.getMorningActivities().contains(activity))
				{
					iterator.remove();
				}
				else
				{
					selectedActivities.remove(activity);
				}
			}
			team.addActivities(selectedActivities);
		});
	}

	private long activityDurationSum(final Set<Activity> activities)
	{
		long sum = activities.stream().mapToLong(activity -> activity.getDuration().toMinutes()).sum();
		return sum;
	}

	/**
	 * Gets activities.
	 *
	 * @return the activities
	 */
	public List<Activity> getActivities()
	{
		return activities;
	}

	/**
	 * Sets activities.
	 *
	 * @param activities
	 * 		the activities
	 */
	public void setActivities(final List<Activity> activities)
	{
		this.activities = activities;
	}
}