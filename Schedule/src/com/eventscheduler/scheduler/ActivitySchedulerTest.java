package com.eventscheduler.scheduler;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.eventscheduler.model.Activity;
import com.eventscheduler.model.Team;

import junit.framework.TestCase;


@RunWith(MockitoJUnitRunner.class)
public class ActivitySchedulerTest extends TestCase
{
	@InjectMocks
	ActivityScheduler activityScheduler=new ActivityScheduler();

	List<Activity> activities;
	@Before
	public void setUp() throws Exception
	{
		activities=new ArrayList<>();
		Activity activity=new Activity();
		activity.setDuration(Duration.ofMinutes(60));
		activity.setName("Digital Tresure Hunt");
		activities.add(activity);

		activity=new Activity();
		activity.setDuration(Duration.ofMinutes(50));
		activity.setName("Buggy Driving");

		activities.add(activity);activity=new Activity();
		activity.setDuration(Duration.ofMinutes(45));
		activity.setName("Scotland Haka");
		activities.add(activity);

		activities.add(activity);activity=new Activity();
		activity.setDuration(Duration.ofMinutes(60));
		activity.setName("Board game");
		activities.add(activity);

		activities.add(activity);activity=new Activity();
		activity.setDuration(Duration.ofMinutes(45));
		activity.setName("Laser tag");
		activities.add(activity);

		activities.add(activity);activity=new Activity();
		activity.setDuration(Duration.ofMinutes(55));
		activity.setName("Catch phrase ");
		activities.add(activity);
		activityScheduler.setActivities(activities);
	}

	@Test
	public void testSchedule()
	{
		final List<Team> schedule = activityScheduler.schedule(2, activities);
		Assert.assertNotNull(schedule);
		assertFalse( schedule.get(1).getMorningActivities().isEmpty());
	}

	@Test
	public void testScheduleActivitiesPreLunch()
	{
		final List<Team> schedule = activityScheduler.schedule(2, activities);
 		activityScheduler.scheduleActivitiesPostLunch(schedule);
		Assert.assertNotNull(schedule);
		Assert.assertNotNull(schedule.get(1).getMorningActivities());

	}

	@Test
	public void testScheduleActivitiesPostLunch()
	{
		final List<Team> schedule = activityScheduler.schedule(2, activities);
		activityScheduler.scheduleActivitiesPostLunch(schedule);
		Assert.assertNotNull(schedule);
		Assert.assertNotNull(schedule.get(0).getAfternoonActivities());

	}
}