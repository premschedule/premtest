package com.eventscheduler.model;

import java.util.HashSet;
import java.util.Set;

import com.eventscheduler.enums.ActivityDuration;



/**
 * The type Team.
 */
public class Team
{
	private Set<Activity> morningActivities = new HashSet<>();
	private Set<Activity> afternoonActivities = new HashSet<>();
	private ActivityDuration duration = ActivityDuration.MORNING;
	private String name;

	/**
	 * Instantiates a new Team.
	 *
	 * @param name
	 * 		the name
	 */
	public Team(String name)
	{
		this.name = name;
	}

	/**
	 * Gets morning activities.
	 *
	 * @return the morning activities
	 */
	public Set<Activity> getMorningActivities()
	{
		return morningActivities;
	}

	/**
	 * Sets morning activities.
	 *
	 * @param morningActivities
	 * 		the morning activities
	 */
	public void setMorningActivities(final Set<Activity> morningActivities)
	{
		this.morningActivities = morningActivities;
	}

	/**
	 * Gets afternoon activities.
	 *
	 * @return the afternoon activities
	 */
	public Set<Activity> getAfternoonActivities()
	{
		return afternoonActivities;
	}

	/**
	 * Sets afternoon activities.
	 *
	 * @param afternoonActivities
	 * 		the afternoon activities
	 */
	public void setAfternoonActivities(final Set<Activity> afternoonActivities)
	{
		this.afternoonActivities = afternoonActivities;
	}

	/**
	 * Gets duration.
	 *
	 * @return the duration
	 */
	public ActivityDuration getDuration()
	{
		return duration;
	}

	/**
	 * Sets duration.
	 *
	 * @param duration
	 * 		the duration
	 */
	public void setDuration(final ActivityDuration duration)
	{
		this.duration = duration;
	}

	/**
	 * Add activities.
	 *
	 * @param activities
	 * 		the activities
	 */
	public void addActivities(Set<Activity> activities)
	{
		this.duration.getActivities(this).addAll(activities);
		this.duration = this.duration.getNextDuration();
	}

	/**
	 * Gets name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets name.
	 *
	 * @param name
	 * 		the name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}
}