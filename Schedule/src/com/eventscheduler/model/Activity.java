package com.eventscheduler.model;

import java.time.Duration;


/**
 * The type Activity.
 */
public class Activity
{
	private String name;
	private Duration duration;

	/**
	 * Instantiates a new Activity.
	 *
	 * @param name
	 * 		the name
	 * @param Duration
	 * 		the duration
	 */
	public Activity(final String name,final  Duration Duration)
	{
		this.name = name;
		this.duration = Duration;
	}

	/**
	 * Instantiates a new Activity.
	 */
	public Activity()
	{

	}

	/**
	 * Gets duration.
	 *
	 * @return the duration
	 */
	public Duration getDuration()
	{

		return duration;
	}

	/**
	 * Gets name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets name.
	 *
	 * @param name
	 * 		the name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Sets duration.
	 *
	 * @param duration
	 * 		the duration
	 */
	public void setDuration(final Duration duration)
	{
		this.duration = duration;
	}
}