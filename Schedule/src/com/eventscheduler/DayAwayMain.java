package com.eventscheduler;

import static com.eventscheduler.util.ActivityConsolePrinter.print;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.eventscheduler.exception.ActivityHandlerException;
import com.eventscheduler.exception.ActivityParseException;
import com.eventscheduler.model.Activity;
import com.eventscheduler.model.Team;
import com.eventscheduler.scheduler.ActivityScheduler;
import com.eventscheduler.util.ActivityFileParser;


/**
 * The type Day away main.
 */
public class DayAwayMain
{
	private static String START_TIME = "9:00 am";
	private static final int NO_OF_TEAMS = 2;
	private static SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
	private static final String DEFAULT_FILE_LOCATION = "src/com/resource/activities.txt";

	/**
	 * The entry point of application.
	 *
	 * @param args
	 * 		the input arguments
	 *
	 * @throws ActivityHandlerException
	 * 		the activity handler exception
	 */
	public static void main(final String[] args) throws ActivityHandlerException
	{
		final List<Activity> activities;
		String fileLocation = DEFAULT_FILE_LOCATION;
		if (!new File(fileLocation).exists())
		{
			System.out.printf("Unable to start up event away day,  File location '%s' is invalid.", fileLocation);
			return;
		}
		try
		{
			activities = new ActivityFileParser(fileLocation).parse();
		}
		catch (IOException e)
		{
			throw new ActivityHandlerException("Unable to parse input activity file: " + fileLocation, e);
		}
		List<Team> teams = new ActivityScheduler().schedule(NO_OF_TEAMS, activities);
		teams.forEach(t -> printSchedule(t));
	}


	/**
	 * Print schedule.
	 *
	 * @param team
	 * 		the team
	 */
	public static void printSchedule(final Team team)
	{
		try
		{
			final Date d = df.parse(START_TIME);
			Calendar cal = Calendar.getInstance();
			cal.setTime(d);

			System.out.println("Activities for " + team.getName());
 			team.getMorningActivities().forEach(a ->
			{
				String stTime = df.format(cal.getTime());
				cal.add(Calendar.MINUTE, (int) a.getDuration().toMinutes());
				print(stTime, a);
			});

 			Activity lunch = new Activity("Lunch", Duration.ofMinutes(60));
			String lunchStTime = df.format(cal.getTime());
			cal.add(Calendar.MINUTE, (int) lunch.getDuration().toMinutes());
			print(lunchStTime, lunch);

 			team.getAfternoonActivities().forEach(a ->
			{
				String afEventTime = df.format(cal.getTime());
				cal.add(Calendar.MINUTE, (int) a.getDuration().toMinutes());
				print(afEventTime, a);
			});

 			Activity presentation = new Activity("Staff Motivation Presentation", Duration.ofMinutes(60));
			String prTime = df.format(cal.getTime());
			cal.add(Calendar.MINUTE, (int) lunch.getDuration().toMinutes());
			print(prTime, presentation);
			System.out.println("\n\n");
		}
		catch (final ParseException e)
		{
			throw new ActivityParseException("Error parsing activity time ", e);
		}
	}
}