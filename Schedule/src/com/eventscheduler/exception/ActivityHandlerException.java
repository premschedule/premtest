package com.eventscheduler.exception;

/**
 * The type Activity handler exception.
 */
public class ActivityHandlerException extends Exception
{

	/**
	 * Instantiates a new Activity handler exception.
	 *
	 * @param msg
	 * 		the msg
	 */
	public ActivityHandlerException(final String msg)
	{
		super(msg);
	}

	/**
	 * Instantiates a new Activity handler exception.
	 *
	 * @param msg
	 * 		the msg
	 * @param t
	 * 		the t
	 */
	public ActivityHandlerException(final String msg, Throwable t)
	{
		super(msg, t);
	}

}
