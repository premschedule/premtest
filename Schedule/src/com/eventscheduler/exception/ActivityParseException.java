package com.eventscheduler.exception;

/**
 * The type Activity parse exception.
 */
public class ActivityParseException extends RuntimeException
{


	/**
	 * Instantiates a new Activity parse exception.
	 *
	 * @param msg
	 * 		the msg
	 */
	public ActivityParseException(final String msg)
	{
		super(msg);
	}

	/**
	 * Instantiates a new Activity parse exception.
	 *
	 * @param msg
	 * 		the msg
	 * @param t
	 * 		the t
	 */
	public ActivityParseException(final String msg, Throwable t)
	{
		super(msg, t);
	}

}
