package com.eventscheduler.enums;

import java.util.HashSet;
import java.util.Set;

import com.eventscheduler.model.Activity;
import com.eventscheduler.model.Team;



/**
 * The enum Activity duration.
 */
public enum ActivityDuration
{

	/**
	 * Morning activity duration.
	 */
	MORNING(150, 180),
	/**
	 * Afternoon activity duration.
	 */
	AFTERNOON(240, 240),
	/**
	 * None activity duration.
	 */
	NONE(0, 0);
	private int minMins;
	private int maxMins;
	private int totalMins = 420;

	ActivityDuration(final int minMins, final int maxMins)
	{
		this.minMins = minMins;
		this.maxMins = maxMins;
	}

	/**
	 * Gets min mins.
	 *
	 * @return the min mins
	 */
	public int getMinMins()
	{
		return minMins;
	}

	/**
	 * Gets max mins.
	 *
	 * @return the max mins
	 */
	public int getMaxMins()
	{
		return maxMins;
	}

	/**
	 * Gets next duration.
	 *
	 * @return the next duration
	 */
	public ActivityDuration getNextDuration()
	{
		switch (this)
		{
			case MORNING:
				return AFTERNOON;
			case AFTERNOON:
				return NONE;
			case NONE:
				return NONE;
		}
		return NONE;
	}

	/**
	 * Gets activities.
	 *
	 * @param team
	 * 		the team
	 *
	 * @return the activities
	 */
	public Set<Activity> getActivities(Team team)
	{
		switch (this)
		{
			case MORNING:
				return team.getMorningActivities();
			case AFTERNOON:
				return team.getAfternoonActivities();
		}
		return new HashSet<>();
	}


	/**
	 * Gets total mins.
	 *
	 * @return the total mins
	 */
	public int getTotalMins()
	{
		return totalMins;
	}
}
